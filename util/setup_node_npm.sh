#!/bin/bash
set -eo pipefail

# Define versions file path
VERSIONS_FILE="./versions.txt"

source util/resolve_os.sh

# Load Node and npm versions from versions.txt
parse_versions() {
    while IFS='=' read -r key value; do
        # Remove any trailing newline characters from the value
        value=$(echo "${value}" | tr -d '\n' | tr -d '\r' | tr -d '\n')
        if [[ "$key" == "NODE" ]]; then
            NODE_VERSION="$value"
        elif [[ "$key" == "NPM" ]]; then
            NPM_VERSION="$value"
        elif [[ "$key" == "NVM" ]]; then
            NVM_VERSION="$value"
        fi
    done < "$VERSIONS_FILE"
}

# Check if the versions file exists
if [ ! -f "$VERSIONS_FILE" ]; then
    echo "Error: versions.txt file not found."
    exit 1
fi

# Parse the versions file
parse_versions

# Install and configure nvm for Node version management
if ! command -v nvm >/dev/null 2>&1; then
  if [[ "$PYTHON" == "python3" ]]; then
    echo "Installing nvm v${NVM_VERSION}..."
    curl -o- "https://raw.githubusercontent.com/nvm-sh/nvm/v${NVM_VERSION}/install.sh" | bash
    export NVM_DIR="$HOME/.nvm"
    [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
  else
    echo "Please install nvm on Windows manually before using this script."
    exit 1
  fi
fi

is_node_installed() {
    nvm ls "$NODE_VERSION" &> /dev/null
}

is_npm_installed() {
    npm --version | grep -q "$NPM_VERSION"
}

# Install Node.js if not installed
if ! is_node_installed; then
    echo "Installing Node $NODE_VERSION with nvm..."
    nvm install "$NODE_VERSION"
    nvm use "$NODE_VERSION"
    nvm alias default "$NODE_VERSION"
else
    echo "Node $NODE_VERSION is already installed."
fi

echo "Switching to Node $NODE_VERSION with nvm..."
if ! nvm use "$NODE_VERSION"; then
    echo "Failed to install Node $NODE_VERSION. Exiting."
    exit 1
fi
nvm alias default "$NODE_VERSION"

# Install npm if not installed or if version doesn't match
if ! is_npm_installed; then
    echo "Installing npm $NPM_VERSION..."
    npm install -g "npm@$NPM_VERSION"
else
    echo "npm $NPM_VERSION is already installed."
fi

