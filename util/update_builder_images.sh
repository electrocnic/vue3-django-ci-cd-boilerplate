#!/bin/bash
set -eo pipefail

VERSIONS_FILE="./versions.txt"

parse_versions() {
    while IFS='=' read -r key value; do
        if [[ "$key" == "DIND" ]]; then
            export DIND_VERSION="$value"
        elif [[ "$key" == "NODE" ]]; then
            export NODE_VERSION="$value"
        elif [[ "$key" == "PYTHON" ]]; then
            export PYTHON_VERSION="$value"
        elif [[ "$key" == "PDM" ]]; then
            export PDM_VERSION="$value"
        fi
    done < "$VERSIONS_FILE"
}

if [ ! -f "$VERSIONS_FILE" ]; then
    echo "Error: versions.txt file not found."
    exit 1
fi

parse_versions

source builder/dind_bash/update_dind_base.sh
source builder/node_cache/update_node_cache.sh
source builder/pip_cache/update_pip_cache.sh
# source builder/cypress/build_cypress_image.sh TODO: reactivate later
