#!/bin/bash
set -eo pipefail

# Define versions file path
VERSIONS_FILE="./versions.txt"

# Source utility script to determine the OS and set $PYTHON variable
source util/resolve_os.sh

# Function to parse versions.txt for Python and PDM versions
parse_versions() {
    while IFS='=' read -r key value; do
        # Remove any trailing newline characters from the value
        value=$(echo "${value}" | tr -d '\n' | tr -d '\r' | tr -d '\n')
        if [[ "$key" == "PYTHON" ]]; then
            PYTHON_VERSION="$value"
        elif [[ "$key" == "PDM" ]]; then
            PDM_VERSION="$value"
        fi
    done < "$VERSIONS_FILE"
}

# Check if the versions file exists
if [ ! -f "$VERSIONS_FILE" ]; then
    echo "Error: versions.txt file not found."
    exit 1
fi

# Parse the versions file
parse_versions

# Install pyenv for Linux
if [[ "$PYTHON" == "python3" ]]; then
    if ! command -v pyenv >/dev/null 2>&1; then
        echo "Installing pyenv..."
        curl https://pyenv.run | bash
        export PYENV_ROOT="$HOME/.pyenv"
        [[ -d $PYENV_ROOT/bin ]] && export PATH="$PYENV_ROOT/bin:$PATH"
        eval "$(pyenv init -)"
        eval "$(pyenv virtualenv-init -)"
    fi

    # List of required packages
    required_packages=(build-essential libssl-dev zlib1g-dev libbz2-dev libreadline-dev libsqlite3-dev curl libncursesw5-dev xz-utils tk-dev libxml2-dev libxmlsec1-dev libffi-dev liblzma-dev)

    # Function to check if a package is installed
    is_installed() {
        dpkg -l "$1" &> /dev/null
    }

    # Check and install missing packages
    missing_packages=()
    for pkg in "${required_packages[@]}"; do
        if ! is_installed "$pkg"; then
            missing_packages+=("$pkg")
        fi
    done

    if [ ${#missing_packages[@]} -ne 0 ]; then
        echo "Installing missing packages: ${missing_packages[*]}"
        sudo apt update
        sudo apt install -y "${missing_packages[@]}"
    fi
else
    echo "Warning: On Windows, please ensure Pyenv for Windows is installed and available in PATH."
fi

# Install Python version using pyenv
if ! pyenv versions | grep -q "${PYTHON_VERSION}"; then
    echo "Installing Python ${PYTHON_VERSION}..."
    pyenv install "${PYTHON_VERSION}"
fi

# Install or update PDM
export PATH=/home/$USER/.local/bin:$PATH
if ! command -v pdm >/dev/null 2>&1; then
    echo "Installing PDM version $PDM_VERSION..."
    curl -sSL https://pdm.fming.dev/install-pdm.py | python3 - -v "$PDM_VERSION"
else
    installed_pdm_version=$(pdm --version | cut -d ' ' -f 3 | tr -d '\n' | tr -d '\r' | tr -d '\n')
    if [[ "$installed_pdm_version" != "$PDM_VERSION" ]]; then
        echo "$installed_pdm_version != $PDM_VERSION"
        echo "Updating PDM to version ${PDM_VERSION}..."
        curl -sSL https://pdm.fming.dev/install-pdm.py | python3 - -v "$PDM_VERSION"
    else
        echo "PDM version $PDM_VERSION is already installed."
    fi
fi

cd "./backend"

# Check for the existence of pyproject.toml
if [ -f "pyproject.toml" ]; then
    # Update pyproject.toml requires-python version
    sed -i "s/requires-python = \".*\"/requires-python = \"==$PYTHON_VERSION\"/" pyproject.toml
    if ! pdm install; then
        echo "pdm install failed. Ensure the pyproject.toml file is healthy and fix any other errors manually."
        exit 1
    fi
else
    echo "pyproject.toml not found. Please initialize a pdm project with 'pdm init -n --python $PYTHON_VERSION'."
    exit 1
fi

