#!/bin/bash
set -eo pipefail

source util/resolve_os.sh

if [ "$1" != "run" ]; then
	./build.sh
fi

cd backend
source .venv/"${VBIN}"/activate

export PORT=8000
echo 'Server running on port ' $PORT
$PYTHON manage.py runserver
