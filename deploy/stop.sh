#!/bin/bash
set -eo pipefail

cd "$(dirname "$0")"

source ../util/scp_ssh_wrapper.sh

ssh_proxy "chmod 755 apps/${NGINX_SERVER_NAME}/app.sh"

echo "Stopping running application"
ssh_proxy "apps/${NGINX_SERVER_NAME}/app.sh stop"

echo "Stopped."
