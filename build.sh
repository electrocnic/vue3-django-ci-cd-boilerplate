#!/bin/bash
set -eo pipefail


. util/resolve_os.sh
util/setup_python_venv.sh
. ./backend/.venv/"${VBIN}"/activate

util/setup_node_npm.sh
cd client

echo 'Run npm build'
npm install
npm run build
echo 'Done...'

cd ../backend

echo 'Collect static'
$PYTHON manage.py collectstatic --noinput
echo 'Done...'

echo 'Run migrations'
$PYTHON manage.py makemigrations
$PYTHON manage.py migrate
echo 'Done...'
