#!/bin/bash
set -eo pipefail

sha256sum "${DIND_BASH_VERSION_SCRIPTS_DIRECTORY}/Dockerfile" | awk '{printf $1}'
