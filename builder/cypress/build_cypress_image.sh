#!/bin/bash
set -eo pipefail

PACKAGE_JSON_PATH="./client/package.json"
CYPRESS_VERSION=$(grep '"cypress":' "$PACKAGE_JSON_PATH" | sed -E 's/.*"cypress": "([^"]+)".*/\1/')

# Check if the version was successfully extracted
if [ -z "$CYPRESS_VERSION" ]; then
    echo "Failed to extract Cypress version from package.json"
    exit 1
fi

docker build \
	--build-arg CLIENT_DIRECTORY \
	--build-arg NODE_CACHE_IMAGE \
	--build-arg CYPRESS_VERSION \
	-t "${CYPRESS_DIND_IMAGE}" \
	-f "${CYPRESS_DIND_VERSION_SCRIPTS_SRC_DIRECTORY}"/Dockerfile .
docker push "${STATIC_FILES_IMAGE}"
