#!/bin/bash
set -eo pipefail

sha256sum "${NODE_CACHE_VERSION_SCRIPTS_DIRECTORY}/Dockerfile" | awk '{printf $1}'
