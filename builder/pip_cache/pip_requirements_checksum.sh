#!/bin/bash
set -eo pipefail

sha256sum "${PIP_CACHE_VERSION_SCRIPTS_DIRECTORY}/pyproject.toml" | awk '{printf $1}'
