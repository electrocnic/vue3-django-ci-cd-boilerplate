#!/bin/bash
set -eo pipefail

# Return 0 if builder image on registry is up to date. return 1 if needs to be updated.
compare_images() {
	builder_pip_sum=$(docker run --entrypoint "${PIP_CACHE_VERSION_SCRIPTS_DIRECTORY}/pip_requirements_checksum.sh" "${PIP_CACHE_IMAGE}")
	repo_pip_sum=$(sha256sum "${BACKEND_DIRECTORY}"/pyproject.toml | awk '{printf $1}')
	echo "Builder's checksum for pyproject.toml was: $builder_pip_sum"
	echo "Repo's checksum for pyproject.toml was:    $repo_pip_sum"
	set +e
	if [ "${builder_pip_sum}" != "${repo_pip_sum}" ]; then
		echo "Checksums did not match."
		return 1;
	else
		set -e
		builder_dockerfile_sum=$(docker run --entrypoint "${PIP_CACHE_VERSION_SCRIPTS_DIRECTORY}/dockerfile_checksum.sh" "${PIP_CACHE_IMAGE}")
		repo_dockerfile_sum=$(sha256sum "${PIP_CACHE_VERSION_SCRIPTS_SRC_DIRECTORY}"/Dockerfile | awk '{printf $1}')
		echo "Builder's checksum for its Dockerfile was: $builder_dockerfile_sum"
		echo "Repo's checksum for its Dockerfile was:    $repo_dockerfile_sum"
		set +e
		if [ "${builder_dockerfile_sum}" != "${repo_dockerfile_sum}" ]; then
			echo "Checksums did not match."
			return 1;
		else
			echo "Checksums did match."
			return 0;
		fi
	fi
}

if [ 0$BUILD -eq 0 ]; then
	set +e
	docker pull "${PIP_CACHE_IMAGE}" 2>/dev/null
	if [ ! $? -eq 0 ]; then
		NOT_FOUND=1
	fi
	set -e

	if [ 0$NOT_FOUND -eq 1 ]; then
		echo "No docker image found in registry, building from scratch..."
		BUILD=1
	else
		set +e
		compare_images
		result=$?
		set -e
		if [ $result -eq 1 ]; then
			echo "Builder out of date, updating image..."
			BUILD=1
		else
			echo "Builder up to date, nothing to do in this stage."
		fi
	fi
fi

if [ 0$BUILD -eq 1 ]; then
	docker build \
		--build-arg PYTHON_VERSION \
		--build-arg PDM_VERSION \
		--build-arg BACKEND_DIRECTORY \
		--build-arg PIP_CACHE_VERSION_SCRIPTS_SRC_DIRECTORY \
		--build-arg PIP_CACHE_VERSION_SCRIPTS_DIRECTORY \
		--build-arg PIP_CACHE_VENV_DIRECTORY \
		-t "${PIP_CACHE_IMAGE}" \
		-f "${PIP_CACHE_VERSION_SCRIPTS_SRC_DIRECTORY}"/Dockerfile .
	docker push "${PIP_CACHE_IMAGE}"
fi
