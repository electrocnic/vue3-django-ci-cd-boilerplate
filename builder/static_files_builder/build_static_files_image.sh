#!/bin/bash
set -eo pipefail

docker build \
	--build-arg BACKEND_DIRECTORY \
	--build-arg CLIENT_DIRECTORY \
	--build-arg PIP_CACHE_IMAGE \
	--build-arg NODE_CACHE_IMAGE \
	-t "${STATIC_FILES_IMAGE}" \
	-f "${BUILDER_VERSION_SCRIPTS_SRC_DIRECTORY}"/Dockerfile .
docker push "${STATIC_FILES_IMAGE}"
